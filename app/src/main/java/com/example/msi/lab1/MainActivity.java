package com.example.msi.lab1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    EditText editText;
    EditText editText_show;
    ArrayList<String> notes = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
    }

    public void clearText(View view) {
        editText = findViewById(R.id.editText);
        editText_show = findViewById(R.id.editText2);
        editText_show.setText(editText.getText());
        notes.add(editText.getText().toString());
        editText.getText().clear();
    }

    public void next_note(View view) {
        if (notes.indexOf(editText_show.getText().toString()) == notes.size()-1) {
            editText_show.setText(notes.get(0));
        } else {
            System.out.println(notes.indexOf(editText_show.getText().toString()));
            editText_show.setText(notes.get(notes.indexOf(editText_show.getText().toString()) + 1));
        }
    }

    public void delete_note(View view) {
        notes.remove(editText_show.getText().toString());
        if (notes.indexOf(editText_show.getText().toString()) == notes.size()-1) {
            editText_show.setText(notes.get(0));
        } else {
            editText_show.setText(notes.get(notes.indexOf(editText_show.getText().toString()) + 1));
        }
    }
}
